using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    float playerHeight = 2.0f;

    [SerializeField] Transform orientation;

    [Header("Movement")]
    [SerializeField] float moveSpeed = 6.0f;
    float movementMultiplier = 10.0f;
    float airMultiplier = 0.2f;

    [Header("Jumping")]
    [SerializeField] float jumpForce = 5.0f;
    [SerializeField] int maxAirJumps = 1;
    int currentAirJumps = 0;


    [Header("Drag")]
    float groundDrag = 6.0f;
    float airDrag = 0.5f;

    [Header("Keybinds")]
    [SerializeField] KeyCode jumpKey = KeyCode.Space;


    [Header("Ground Detection")]
    [SerializeField] LayerMask groundMask;
    float horizontalMovement;
    float verticalMovement;

    bool isGrounded;
    float groundDistance = 0.5f;

    Vector3 moveDirection;
    Vector3 slopeMoveDirection;

    new Rigidbody rigidbody;

    RaycastHit slopeHit;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.freezeRotation = true;
    }


    void Update()
    {
        isGrounded = Physics.CheckSphere(transform.position - new Vector3(0, playerHeight / 2, 0), groundDistance, groundMask);
        HandleInput();
        ControlDrag();

        if (Input.GetKeyDown(jumpKey))
        {
            if (isGrounded || (!isGrounded && currentAirJumps < maxAirJumps))
            {
                Jump();
            }
        }

        slopeMoveDirection = Vector3.ProjectOnPlane(moveDirection, slopeHit.normal);
    }

    void FixedUpdate()
    {
        MovePlayer();
    }

    private void Jump()
    {
        if (!isGrounded)
        {
            currentAirJumps++;
        }
        rigidbody.AddForce(transform.up * jumpForce, ForceMode.Impulse);
    }

    void HandleInput()
    {
        horizontalMovement = Input.GetAxisRaw("Horizontal");
        verticalMovement = Input.GetAxisRaw("Vertical");

        moveDirection = orientation.forward * verticalMovement + orientation.right * horizontalMovement;
    }

    void ControlDrag()
    {
        if (isGrounded)
        {
            currentAirJumps = 0;
            rigidbody.drag = groundDrag;
        }
        else
        {
            rigidbody.drag = airDrag;
        }
    }

    void MovePlayer()
    {
        if (isGrounded && !OnSlope())
        {
            rigidbody.AddForce(moveDirection.normalized * moveSpeed * movementMultiplier, ForceMode.Acceleration);
        }
        else if (isGrounded && OnSlope())
        {
            rigidbody.AddForce(slopeMoveDirection.normalized * moveSpeed * movementMultiplier, ForceMode.Acceleration);
        }
        else
        {
            rigidbody.AddForce(moveDirection.normalized * moveSpeed * movementMultiplier * airMultiplier, ForceMode.Acceleration);
        }
    }

    bool OnSlope()
    {
        if (Physics.Raycast(transform.position, Vector3.down, out slopeHit, playerHeight / 2 + 0.5f))
        {
            if (slopeHit.normal != Vector3.up)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else return false;
    }

}
